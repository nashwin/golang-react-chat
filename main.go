package main

import (
	"fmt"
	"net/http"
	"os"

	"github.com/gorilla/mux"
)

var (
	PORT = os.Getenv("PORT")
)

func main() {
	r := mux.NewRouter()
	r.PathPrefix("/assets").Handler(http.StripPrefix("/assets", http.FileServer(http.Dir("./ui/dist/assets"))))

	r.Handle("/", http.FileServer(http.Dir("./ui/dist")))
	if PORT == "" {
		PORT = "4444"
	}
	fmt.Printf("Server starting at PORT: %s", PORT)

	http.ListenAndServe(fmt.Sprintf(":%s", PORT), r)
}
